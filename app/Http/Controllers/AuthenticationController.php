<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Services\Rest;

use Firebase\JWT\JWT;

class AuthenticationController extends Controller
{
    private $email;
    private $passcode;
    private $customerName;
    private $taxId;
    private $attempts;
    private $firstLogin;
    private $pinResetCheck;
    private $registrationStatusID;
    private $registrationStatus;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function authenticateUser(Request $request) {

        //Validate Request
        $request->validate([
            'email' => 'required',
            'passcode' => 'required',
        ]);

        $rest = new Rest();

        $resultArray = DB::table('customer')->select('customerName','passcode','email','taxId','attempts','pinResetCheck','firstLogin','registrationStatusID')->where('email', $request->email)->get();

        $this->attempts = $resultArray[0]->attempts ?? 0;

        $tempPasscode = $resultArray[0]->passcode ?? '';

        $this->pinResetCheck = $resultArray[0]->pinResetCheck ?? 1;

        $this->firstLogin = $resultArray[0]->firstLogin ?? 0;

        $this->registrationStatus = $resultArray[0]->registrationStatusID ?? '';

        $resultStatus = false;

        if ($resultArray->isEmpty()) {
            return response(array('message' => 'invalid email or passcode'), 401)->header('Content-Type', 'application/json');
        } else if(($request->email == $resultArray[0]->email) && password_verify($request->passcode, $tempPasscode)){

            if ($resultArray[0]->registrationStatusID == 4 || $resultArray[0]->registrationStatusID == 3 || $resultArray[0]->registrationStatusID == 5){
                return response(array('message' => 'Unable to action request at this time. Please contact the Victoria Mutual Group.'), 401)->header('Content-Type', 'application/json');
            }

            $storedPasscode = $resultArray[0]->passcode;

            $this->attempts = $resultArray[0]->attempts;

            $this->taxId = $resultArray[0]->taxId;

            $this->customerName = $resultArray[0]->customerName;

            $this->firstLogin = $resultArray[0]->firstLogin;


            if($this->attempts < 3 && $this->firstLogin == 0){
                $this->logLogin($request->email);
                $resultStatus = true;
            }else if ($this->attempts < 3 && $this->firstLogin == 1) {
                $this->updateUser($request->email);
                $resultStatus = true;
            }
        } else if ($this->attempts > 2) {
            $this->lockUser($request->email);
            return response(array('message' => 'account locked, please click forget your passcode for help.'), 403)->header('Content-Type', 'application/json');
        } else if ($this->convertToBool($this->firstLogin)){
            $this->attempts = $this->attempts + 1;
            $this->updateDB($this->attempts, 1, $request->email);
            return response(array('message' => 'invalid email or passcode'), 401)->header('Content-Type', 'application/json');
        } else {
            $this->attempts = $this->attempts + 1;
            $this->updateDB($this->attempts, 0, $request->email);
            $passcodeAttempts = 3 - $this->attempts;
            return response(array('message' => "invalid email or passcode, you now have $passcodeAttempts attempts left."), 401)->header('Content-Type', 'application/json');
        }


        if($resultStatus) {
            return $this->createToken($request->email, $this->taxId, $this->customerName, $this->pinResetCheck, $this->firstLogin, $this->registrationStatus);
        }else{
            return response(array('message' => 'invalid email or passcode'), 401)->header('Content-Type', 'application/json');
        }
    }

    public function logLogin($email){

        $firstLogin = 0;
        $attempts = 0;

        // create query
        DB::table('customer')->where('email', $email)->update(['attempts' => $attempts, 'firstLogin' => $firstLogin, 'lastLogin' => now()]);
    }


    public function updateUser($email){

        $registrationStatusID = 2;
        $pinResetCheck = 0;

        DB::table('customer')->where('email', $email)->update(['registrationStatusID' => $registrationStatusID, 'pinResetCheck' => $pinResetCheck, 'lastLogin' => now()]);

    }

    public function updateDB($attempts, $firstLogin, $email){

        $this->pinResetCheck = false;

        $lastLogin = Date("Y-m-d h:i:s");

        DB::table('customer')->where('email', $email)->update(['attempts' => $attempts, 'firstLogin' => $firstLogin]);
    }


    public function lockUser($email){

        $pinResetCheck = 1;

        // create query
        DB::table('customer')->where('email', $email)->update(['pinResetCheck' => $pinResetCheck]);
    }


    private function convertToBool($value){
        switch ($value) {
            case true:
                return 1;
              break;
            case false:
                return 0;
              break;
        }
    }

    private function convertStatus($value){
        switch ($value) {
            case 1:
                return 'NEW';
              break;
            case 2:
                return 'RESOLVED';
            case 3:
                return 'CANCELLED';
            case 4:
                return 'BLOCKED';
            case 5:
                return 'UNRESOLVED';
              break;
            default:
            exit;
        }
    }

    public function createToken($email, $taxId, $customerName, $pinResetCheck, $firstLogin, $registrationStatus) {
        try {

            $secret = env('TOKEN_SECRET_KEY');

            $payload = array(
                "email" => $email,
                "expiry" => 300000,
                "taxID" => $taxId,
                "name" => $customerName,
                "userStatus" => $this->convertToBool($pinResetCheck),
                "firstLogin" => $this->convertToBool($firstLogin),
                "registrationStatus" => $this->convertStatus($registrationStatus),
                "created" => date("Y-m-d H:i:s"),
            );

            $token = JWT::encode($payload, $secret);

            return response(array('token' => $token), 200)->header('Content-Type', 'application/json');
        } catch (Exception $e) {
            return response(array('message' => $e->getMessage()), 400)->header('Content-Type', 'application/json');
        }
    }
}
