<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChangeStatusController extends Controller
{

    private $taxId;
    private $status;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

    public function check() {

        $resultArray = DB::table('customer')->select('taxId')->where('taxId', $this->taxId)->get();

        $resultStatus = false;

        if (!$resultArray->isEmpty()) {
            $resultStatus = true;
        }

        return $resultStatus;
    }

    public function update(Request $request) {

        //Validate Request
        $request->validate([
            'taxId' => 'required',
            'status' => 'required',
        ]);

        $this->status = $request->status;

        $this->taxId = $request->taxId;

        $newStatus = $this->convertStatus($this->status);

        $pinResetCheck = 0;

        $firstLogin = 1;

        if(strtoupper($this->status) == 'RESOLVED'){
            $pinResetCheck = 1;
        }

        $statement = DB::table('customer')->where('taxId', $this->taxId)->update(['registrationStatusID' => $newStatus, 'pinResetCheck' => $pinResetCheck, 'firstLogin' => $firstLogin]);

        if($this->check()){
            // execute query
            if($statement){;
                return response(array('message' => 'User Status Updated'), 200)->header('Content-Type', 'application/json');
            }
        }

        return response(array('message' => 'Unable to Update'), 200)->header('Content-Type', 'application/json');
    }


    private function convertStatus($value){
        $value = strtoupper($value);
        switch ($value) {
            case 'NEW':
                return 1;
              break;
            case 'RESOLVED':
                return 2;
            case 'CANCELLED':
                return 3;
            case 'CANCELED':
                return 3;
            case 'BLOCKED':
                return 4;
            case 'UNRESOLVED':
                return 5;
              break;
            default:
            http_response_code(400);
            header("Content-Type: application/json");
            echo json_encode(
                array('message' => 'Unable to update status')
            );
            exit;
        }
    }

}
