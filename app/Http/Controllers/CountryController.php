<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $result = Country::all();

            $rows = $result->count();

            if($rows > 0){
                $post_arr = array();
                $post_arr['country'] = array();
                foreach ($result as $country) {
                    array_push($post_arr['country'], $country->country);
                }
                return response()->json($post_arr);
            }else{
                return response(array('message' => 'no country found'), 400)->header('Content-Type', 'application/json');
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return response(array('message' => 'no country found'), 400)->header('Content-Type', 'application/json');
        } catch (Exception $e) {
            return response(array('message' => 'no country found'), 400)->header('Content-Type', 'application/json');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
