<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CustomerType;

class CustomerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $result = CustomerType::all();

            $rows = $result->count();

            if($rows > 0){

                $post_arr = array();
                $post_arr['customerType'] = array();
                foreach ($result as $type) {
                    array_push($post_arr['customerType'], $type->customerType);
                }

                return response()->json($post_arr);
            }else{
                return response(array('message' => 'no customer type found'), 400)->header('Content-Type', 'application/json');
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return response(array('message' => 'no customer type found'), 400)->header('Content-Type', 'application/json');
        } catch (Exception $e) {
            return response(array('message' => 'no customer type found'), 400)->header('Content-Type', 'application/json');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
