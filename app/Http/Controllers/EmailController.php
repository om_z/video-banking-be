<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Email;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function send(Request $request) {

        //Validate Request
        $request->validate([
            'email' => 'required',
            'name' => 'required'
        ]);

        $emailResult = new Email();

        if($emailResult->sendMail($request->email,$request->name)){
            return response(array('status' => 'success', 'response' => 'mail sent!'), 200)->header('Content-Type', 'application/json');
        }else{
            return response(array('status' => 'failed','response' => 'failed to notify customer'), 401)->header('Content-Type', 'application/json');
        }

    }
}
