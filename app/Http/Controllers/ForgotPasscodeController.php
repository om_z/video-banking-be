<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Services\Email;

class ForgotPasscodeController extends Controller
{
    private $hash;
    private $customerName;
    public $email;
    private $password;
    private $registrationStatusID;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateDB($passcode){

        $attempts = 0;
        $pinResetCheck = true;
        $firstLogin = true;

        // create query
        $statement = DB::table('customer')->where('email', $this->email)->update(['passcode' => $passcode, 'attempts' => $attempts, 'pinResetCheck' => $pinResetCheck, 'firstLogin' => $firstLogin]);

        // execute query
        if($statement){
            $this->notifyCustomer($this->email);
            return true;
        }


        // print error if something goes wrong
        printf("Error %s. \n", $statement->error);
        return false;
    }


    public function verify(Request $request) {

        //Validate Request
        $request->validate([
            'email' => 'required',
        ]);

        $this->email = $request->email;

        $resultArray = DB::table('customer')->select('customerName', 'email', 'registrationStatusID')->where('email', $this->email)->get();

        $resultStatus = false;

        if(!$resultArray->isEmpty()){

            $this->customerName = $resultArray[0]->customerName;
            $this->email = $resultArray[0]->email;
            $this->registrationStatusID = $resultArray[0]->registrationStatusID;

            if($this->registrationStatusID == 2) {
                $this->hash = $this->generatePasscode();
                $this->password = $this->hash;
                $this->hash = password_hash($this->hash,PASSWORD_DEFAULT,['cost' => 12]);
                $this->updateDB($this->hash);
                $resultStatus = true;
            } else if($this->registrationStatusID == 4 || $this->registrationStatusID == 3) {
                return response(array('message' => 'Unable to action request at this time. Please contact the Victoria Mutual Group.'), 400)->header('Content-Type', 'application/json');
            }
        }

        if($resultStatus){
            return response(array('message' => 'Passcode reset was successful.'), 200)->header('Content-Type', 'application/json');
    
        }else{
            return response(array('message' => 'Passcode reset was not successful.'), 400)->header('Content-Type', 'application/json');
        }
    }



    private function generatePasscode(){
        $length = 12;

        $add_dashes = false;

        $available_sets = 'lud';

        $sets = array();

        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '1234567890';
    
        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
    
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
    
        $password = str_shuffle($password);
    
        if(!$add_dashes)
            return $password;
    
        $dash_len = floor(sqrt($length));
        $passcode = '';
        while(strlen($password) > $dash_len)
        {
            $passcode .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $passcode .= $password;
        return $passcode;
    }


    public function notifyCustomer($email){
        $emailResult = new Email();

        if($emailResult->sendPasscodeMail($email, $this->customerName, $this->password)){
            return true;
        }else{
            return false;
        }
    }
}
