<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Services\Rest;

class PasscodeResetController extends Controller
{
    private $email;
    private $passcode;
    private $customerName;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function resetUser(Request $request){

        //Validate Request
        $request->validate([
            'email' => 'required',
            'passcode' => 'required',
        ]);

        $rest = new Rest();

        $resultArray = DB::table('customer')->select('customerName','registrationStatusID')->where('email', $request->email)->get();

        $this->email = $request->email;

        $resultStatus = false;

        if (!$resultArray->isEmpty() && $resultArray[0]->registrationStatusID == 2){
            $this->passcode = password_hash($request->passcode,PASSWORD_DEFAULT,['cost' => 12]);
            $this->resetPassword($this->passcode);
            $resultStatus = true;
        }

        if($resultStatus){
            return response(array('message' => 'Passcode Reset Successful.'), 200)->header('Content-Type', 'application/json');
        }

        return response(array('message' => 'Passcode reset was not successful.'), 401)->header('Content-Type', 'application/json');
    }

    public function resetPassword($passcode){

        $firstLogin = 0;
        $registrationStatusID = 2;
        $pinResetCheck = 0;
        $attempts = 0;

        DB::table('customer')->where('email', $this->email)->update(['passcode' => $passcode, 'pinResetCheck' => $pinResetCheck, 'registrationStatusID' => $registrationStatusID, 'attempts' => $attempts, 'lastLogin' => now(), 'firstLogin' => $firstLogin]);
    }
}
