<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class RecaptchaController extends Controller
{

    private $url = 'https://www.google.com/recaptcha/api/siteverify';
    private $completedURL;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {}


    /**
     * Get recaptcha google status resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCaptchaStatus(Request $request)
    {

        //Validate Request
        $request->validate([
            'response' => 'required'
        ]);

        $secret = env('RECAPTCHA_SERVER_KEY');

        $this->completedURL = $this->url.'?secret='.$secret.'&response='.$request->response;

        $json = array(
          "response" => '',
        );
        
        $postdata = json_encode($json);
        
        $ch = curl_init($this->completedURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        
        // Check HTTP status code
        if (!curl_errno($ch)) {
            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
              case 200:
                return response($result)->header('Content-Type', 'application/json');
                break;
              default:
                return response($result, $http_code)->header('Content-Type', 'application/json');
            }
          }
          
          // Close handle
          curl_close($ch);

        // return $request->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
