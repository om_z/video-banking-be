<?php

namespace App\Http\Controllers;

use App\Services\VmService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Customer;

// include(app_path() . '/Services/VmRegister.php');


class RegistrationController extends Controller
{

    private $customerName;
    private $email;
    private $taxId;
    private $passcode;
    private $taxIdType;
    private $dob;
    private $termsAccepted;
    private $captchaChallenged;
    private $customerType;  
    private $pinResetCheck;
    private $registrationStatus;
    private $attempts;
    private $lastLogin;
    private $phoneNumber;
    private $hash;

    private $accountNumber;
    private $taxIdName;
    private $issuingCountry;

    private $vmRegister;
    private $idTypeID;
    private $customerTypeID;
    private $firstLogin;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate Request
        $request->validate([
            'customerName' => 'required',
            'email' => 'required',
            'taxId' => 'required',
            'taxIdType' => 'required',
            'dob' => 'required',
            'termsAccepted' => 'required',
            'captchaChallenged' => 'required',
            'customerType' => 'required',
            'phoneNumber' => 'required',
            'taxIdName' => 'nullable',
            'issuingCountry' => 'nullable',
            'accountNumber' => 'nullable'
        ]);

        $this->vmRegister = new VmService();

        $this->passcode = $this->generatePasscode();

        $this->pinResetCheck = 0;

        $this->attempts = 0;

        $this->lastLogin = now();

        $this->registrationStatus = 1;

        $this->firstLogin = true;

        // echo $this->passcode;

        $this->hash = password_hash($this->passcode,PASSWORD_DEFAULT,['cost' => 12]);
        
        $this->idTypeID = $this->getTaxIdType($request->taxIdType);

        $this->customerTypeID = $this->getCustomerIdType($request->customerType);

        $this->termsAccepted = $this->convertBool($request->termsAccepted);
        $this->captchaChallenged = $this->convertBool($request->captchaChallenged);

        if ($this->reRegisterCustomer($request->taxId, $request->email)) {
            // delete user if status unresolved or cancelled
            $this->deletUser($request->taxId, $request->email);
        }


        // execute query
        try {
            DB::table('customer')->insert([
                'customerName' => $request->customerName,
                'email' => $request->email,
                'passcode' =>  $this->hash,
                'taxId' =>  $request->taxId,
                'idTypeID' =>  $this->idTypeID,
                'dob' =>  $request->dob,
                'termsAccepted' =>  $request->termsAccepted,
                'captchaChallenged' =>  $this->captchaChallenged,
                'customerTypeID' =>  $this->customerTypeID,
                'pinResetCheck' =>  $this->pinResetCheck,
                'attempts' =>  $this->attempts,
                'lastLogin' =>  $this->lastLogin,
                'firstLogin' =>  $this->firstLogin,
                'registrationStatusID' =>  $this->registrationStatus,
                'accountNumber' =>  $this->accountNumber,
                'taxIdName' =>  $this->taxIdName,
                'issuingCountry' =>  $this->issuingCountry
            ]);

            //send off to vm
            $this->vmRegister->updateCustomer($request->customerName, $request->email, $request->taxIdType, $request->taxId, $request->phoneNumber, $this->passcode, $request->customerType);

            return response(array('message' => 'account created'), 200)->header('Content-Type', 'application/json');
            } catch (\Illuminate\Database\QueryException $e) {
            if ($e->errorInfo[1] == 1062) {
                // duplicate entry, do something else
                return response(array('message' => 'Please ensure that you have not registered before with this information or review your information entered and resubmit.'), 401)->header('Content-Type', 'application/json');
            } else {
                // an error other than duplicate entry occurred
                return response(array('message' => 'Please review your registration information entered and resubmit.'), 400)->header('Content-Type', 'application/json');
                
            }
        }
        return response(array('message' => 'Please review your registration information entered and resubmit.'), 400)->header('Content-Type', 'application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reRegisterCustomer($taxId,$email){

        $returnedValue = DB::table('customer')->where('taxId', $taxId)->orWhere('email', $email)->value('registrationStatusID');

        $resultStatus = false;

        if ($returnedValue == 5 || $returnedValue == 3) {
            $resultStatus = true;
        }

        return $resultStatus;
    }

    private function deletUser($taxId,$email){
        DB::table('customer')->where('taxId', $taxId)->orWhere('email', $email)->delete();
    }

    private function getTaxIdType($idType){
        switch ($idType) {
            case 'TRN':
                return 1;
              break;
            case 'SSN':
                return 2;
              break;
            case 'NI':
                return 3;
              break;
            case 'SIN':
                return 4;
              break;
            case 'OTHER':
                return 5;
              break;
        }
    }

    private function getCustomerIdType($idType){

        switch ($idType) {
            case 'VMBS Member Only':
                return 1;
              break;
            case 'VMWM Client Only':
                return 2;
              break;
            case 'Both VMBS and VMWM Customer':
                return 3;
              break;
            case 'Prospect':
                return 4;
              break;
        }
    }

    private function convertBool($value){
        switch ($value) {
            case true:
                return 1;
              break;
            case false:
                return 0;
              break;
        }
    }


    private function generatePasscode(){
        $length = 12;

        $add_dashes = false;

        $available_sets = 'lud';

        $sets = array();

        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '1234567890';
    
        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
    
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
    
        $password = str_shuffle($password);
    
        if(!$add_dashes)
            return $password;
    
        $dash_len = floor(sqrt($length));
        $passcode = '';
        while(strlen($password) > $dash_len)
        {
            $passcode .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $passcode .= $password;
        return $passcode;
    }

}
