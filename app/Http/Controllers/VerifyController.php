<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Firebase\JWT\JWT;

class VerifyController extends Controller
{

    private $customerName;
    private $email;
    private $password;
    private $taxId;
    private $firstLogin;
    private $pinResetCheck;
    private $registrationStatus;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verifyUser(Request $request)
    {
        //Validate Request
        $request->validate([
            'email' => 'required',
        ]);
  
        $resultArray = DB::table('customer')->select('customerName', 'email', 'taxId', 'firstLogin', 'pinResetCheck', 'registrationStatusID')->where('email', $request->email)->get();

        $resultStatus = false;

        if (!$resultArray->isEmpty()) {

            $this->customerName = $resultArray[0]->customerName ?? '';

            $this->email = $resultArray[0]->email ?? '';

            $this->registrationStatus = $resultArray[0]->registrationStatusID ?? '';

            $this->taxId = $resultArray[0]->taxId ?? 0;

            $this->firstLogin = $resultArray[0]->firstLogin ?? 0;

            $this->pinResetCheck = $resultArray[0]->pinResetCheck ?? 0;

            $this->pinResetCheck = $this->convertToBool($this->pinResetCheck);

            $this->firstLogin = $this->convertToBool($this->firstLogin);

            return $this->createToken($this->email, $this->taxId, $this->customerName, $this->pinResetCheck, $this->firstLogin);
        }

        return response(array('message' => 'Please wait until we reach out to you or review your information entered and resubmit.'), 401)->header('Content-Type', 'application/json');
    }

    private function convertStatus($value){
        switch ($value) {
            case 1:
                return 'NEW';
              break;
            case 2:
                return 'RESOLVED';
            case 3:
                return 'CANCELLED';
            case 4:
                return 'BLOCKED';
            case 5:
                return 'UNRESOLVED';
              break;
            default:
            exit;
        }
    }

    private function convertToBool($value){
        switch ($value) {
            case true:
                return 1;
              break;
            case false:
                return 0;
              break;
        }
    }

    public function createToken($email, $taxId, $customerName, $pinResetCheck, $firstLogin) {
        try {

            $secret = env('TOKEN_SECRET_KEY');

            $payload = array(
                "email" => $email,
                "expiry" => 300000,
                "taxID" => $taxId,
                "name" => $customerName,
                "userStatus" => $pinResetCheck,
                "firstLogin" => $firstLogin,
                "registrationStatus" => $this->convertStatus($this->registrationStatus),
                "created" => date("Y-m-d H:i:s"),
            );

            $token = JWT::encode($payload, $secret);

            return response(array('token' => $token), 200)->header('Content-Type', 'application/json');
        } catch (Exception $e) {
            return response(array('message' => $e->getMessage()), 400)->header('Content-Type', 'application/json');
        }
    }
}
