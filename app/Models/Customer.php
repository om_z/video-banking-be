<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer';

    protected $fillable = [
        'customerName',
        'email',
        'passcode',
        'taxId',
        'idTypeID',
        'dob',
        'termsAccepted',
        'captchaChallenged',
        'customerTypeID',
        'pinResetCheck',
        'attempts',
        'lastLogin',
        'firstLogin',
        'registrationStatusID',
        // 'accountNumber',
        // 'taxIdName',
        // 'issuingCountry'
    ];

    use HasFactory;
}
