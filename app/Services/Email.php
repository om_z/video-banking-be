<?php

    namespace App\Services;

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    //Load Composer's autoloader
    // require 'vendor/autoload.php';
    // require '../../vendor/autoload';

    class Email {

        protected $sender = "omri.hopson@gmail.com";
        protected $userName = "omri.hopson@gmail.com";
        protected $password = "rssyxrbfixkvotpe";
        protected $host = "smtp.gmail.com";
        protected $port = 465;
        protected $subject = "Registration Confirmation";
        protected $resetSubject = "Passcode Reset";
        protected $receiver = "";

        public function sendMail($receiver,$name) {

            try {

                $registrationTemplate = '<!DOCTYPE html><html> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <title></title> <style>.column-1, .column-2{float: left; width: 45%;}.column-2{margin-left: 10%;}.hidden-on-large{display: none;}/* On screens that are 900px wide or less, go from two(2) columns to one(1) columns */ @media screen and (max-width: 900px){.column-1, .column-2{width: 100%; float: none;}.column-2{margin-left: 0px;}.hidden-on-large{display: block;}}</style> </head> <body style="font-family: Arial, HelveticaNeue, Helvetica Neue; color: #808080;"> <div id="header" style="background-color: #f0403d;height: 50px;width: 100%;"></div><div style="width: 90%; max-width: 650px; margin: 0 auto; margin-top: 15px; margin-bottom: 50px; text-align: center;"> <img width="250px" alt="headerImage" src="cid:my-logo"> </div><div style="width: 90%; max-width: 650px; margin: 0 auto;"> <p style="font-size: 16px; margin-bottom: 25px; margin-top: 0px;">Hi '. $name.' !</p><div> <p style="font-size: 16px; margin-bottom: 25px; margin-top: 0px;"> Thank you for your interest in using <b>VM Express Video Banking.</b> We are reviewing your application and will advise you of the result within 24 hours. </p><p style="font-size: 16px; margin-bottom: 25px; margin-top: 0px;"> We are excited to offer you yet another way to stay in touch and get much of the information you need to keep your banking needs on track. Here are some of the options available with VM Express Video Banking. </p></div></div><div class="row" style="width: 90%; max-width: 650px; margin: 0 auto; margin-top: 30px; min-height: 400px;"> <div class="column-1" style="margin-bottom: 25px;"> <h4 style="font-size: 17px; margin-top: 0px; margin-bottom: 10px; color: #585858;">Service Assistance</h4> <p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Account Balance Verification</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">General Queries</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Multilink Transactions Queries</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Financial Consultation</p><hr style="background: #4fB0B3; margin-top: 20px; margin-bottom: 20px; border: 1px solid #4FB0B3;"> <h4 style="font-size: 17px; margin-top: 0px; margin-bottom: 10px; color: #585858;">Product Related Enquiry </h4> <p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Mortgages</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Other Loans</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Savings</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Wealth Management &amp; Investments</p></div><div class="column-2" style="margin-bottom: 25px;"> <hr class="hidden-on-large" style="background: #4fB0B3; margin-top: 20px; margin-bottom: 20px; border: 1px solid #4FB0B3;"> <h4 style="font-size: 17px; margin-top: 0px; margin-bottom: 10px; color: #585858;">Loan Pre-Qualification &amp; Application</h4> <p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Mortgage Loan Application</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Unsecured Loan Pre-Qualification</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Unsecured Loan Application</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Auto Loan Pre-Qualification</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Auto Loan Application</p><hr style="background: #4fB0B3; margin-top: 20px; margin-bottom: 20px; border: 1px solid #4FB0B3;"> <h4 style="font-size: 17px; margin-top: 0px; margin-bottom: 10px; color: #585858;">Information Maintenance </h4> <p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Name Change</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Address Change</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Email Address Change</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Date of Birth Change</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Next of KIN</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">References</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Proxy</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Mother&apos;s Maiden Name</p></div></div><div style="width: 90%; max-width: 650px; margin: 0 auto; margin-bottom: 150px;"> <p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;"> If you need additional support, we would be delighted to guide you further through our Member Engagement Centre at 876-754-8627 or via the live chat option available on www.vmbs.com </p><br><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Regards,</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;"><b>VM Express Video Banking.</b></p></div><footer id="footer" style="background-color: #f0403d; height: 100px; width: 100%; margin-top: 30px;"></footer> </body></html>';

                //smtp settings

                $mail = new PHPMailer();
                $mail->IsSMTP();
                // $mail->SMTPDebug = 1;
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = 'ssl';
                $mail->Host = $this->host;
                $mail->Port = $this->port;
                $mail->IsHTML(true);
                $mail->Username = $this->userName;
                $mail->Password = $this->password;
                $mail->SetFrom($this->sender);
                $mail->Subject = $this->subject;
                $mail->AddEmbeddedImage('../resources/images/logo.png', 'my-logo', 'my-logo.png '); 
                $mail->Body = $registrationTemplate;
                $mail->AddAddress($receiver);

                if($mail->send()){
                    return true;
                }else{
                    return false;
                }

            }catch (Exception $error) {
                http_response_code(500);
                header("Content-Type: application/json");
                echo json_encode(
                    array(
                        'status' => 'failed',
                        'response' => 'Server Error'
                        )
                );


            }
        }


        public function sendPasscodeMail($receiver, $name, $passcode) {

          try {


              $passcodeTemplate = '<!DOCTYPE html><html> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <title></title> <style>.column-1, .column-2{float: left; width: 45%;}.column-2{margin-left: 10%;}.hidden-on-large{display: none;}/* On screens that are 900px wide or less, go from two(2) columns to one(1) columns */ @media screen and (max-width: 900px){.column-1, .column-2{width: 100%; float: none;}.column-2{margin-left: 0px;}.hidden-on-large{display: block;}}</style> </head> <body style="font-family: Arial, HelveticaNeue, Helvetica Neue; color: #808080;"> <div id="header" style="background-color: #f0403d;height: 50px;width: 100%;"></div><div class="row" style="width: 90%; max-width: 650px; margin: 0 auto; margin-top: 30px; min-height: 50px;"> </div><div style="width: 90%; max-width: 650px; margin: 0 auto; margin-top: 15px; margin-bottom: 110px; text-align: center;"> <img width="250px" alt="headerImage" src="cid:my-logo"> </div><div style="width: 90%; max-width: 650px; margin: 0 auto;"> <p style="font-size: 16px; margin-bottom: 25px; margin-top: 0px;">Hi '. $name.' !</p><div> <p style="font-size: 16px; margin-bottom: 25px; margin-top: 0px;"> We are sending you this email because you have requested a passcode reset on our <b>VM Express Video Banking</b> platform. </p><p style="font-size: 16px; margin-bottom: 25px; margin-top: 0px;"> Please use temporary code: <b>'. $passcode.'</b> to reset your passcode. </p></div></div><div class="row" style="width: 90%; max-width: 650px; margin: 0 auto; margin-top: 30px; min-height: 100px;"> </div><div style="width: 90%; max-width: 650px; margin: 0 auto; margin-bottom: 30px;"> <p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;"> If you need additional support, we would be delighted to guide you further through our Member Engagement Centre at 876-754-8627 or via the live chat option available on www.vmbs.com </p><br><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;">Regards,</p><p style="font-size: 16px; margin-bottom: 3px; margin-top: 0px;"><b>VM Express Video Banking.</b></p></div><footer id="footer" style="background-color: #f0403d; height: 100px; width: 100%; margin-top: 30px;"></footer> </body></html>';

              //smtp settings

              $mail = new PHPMailer();
              $mail->IsSMTP();
              // $mail->SMTPDebug = 1;
              $mail->SMTPAuth = true;
              $mail->SMTPSecure = 'ssl';
              $mail->Host = $this->host;
              $mail->Port = $this->port;
              $mail->IsHTML(true);
              $mail->Username = $this->userName;
              $mail->Password = $this->password;
              $mail->SetFrom($this->sender);
              $mail->Subject = $this->resetSubject;
              $mail->AddEmbeddedImage('../resources/images/logo.png', 'my-logo', 'my-logo.png '); 
              $mail->Body = $passcodeTemplate;
              $mail->AddAddress($receiver);

              if($mail->send()){
                  return true;
              }else{
                  return false;
              }

          }catch (Exception $error) {
              http_response_code(500);
              header("Content-Type: application/json");
              echo json_encode(
                  array(
                      'status' => 'failed',
                      'response' => 'Server Error'
                      )
              );


          }
      }


    }

?>