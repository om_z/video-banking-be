<?php


	namespace App\Services;

	// include_once('../config/constants.php');
	// include_once('../../vendor/autoload.php');

    use Firebase\JWT\JWT;

    class Rest {

        public function __construct() {
			if($_SERVER['REQUEST_METHOD'] !== 'POST') {
				$this->throwError(400, 'Request Method is not valid.');
            }

            if($_SERVER['CONTENT_TYPE'] !== 'application/json') {
				$this->throwError(400, 'Request content type is not valid');
            }

            // check for header field

			$token = $this->getBearerToken();
			
			$this->verifyBearerToken($token);

        }


	    // Get hearder Authorization

	    public function getAuthorizationHeader(){
	        $headers = null;
	        if (isset($_SERVER['Authorization'])) {
	            $headers = trim($_SERVER["Authorization"]);
	        }
	        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
	            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
	        } elseif (function_exists('apache_request_headers')) {
	            $requestHeaders = apache_request_headers();
	            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
	            if (isset($requestHeaders['Authorization'])) {
	                $headers = trim($requestHeaders['Authorization']);
	            }
	        }
	        return $headers;
        }

	    public function getBearerToken() {
	        $headers = $this->getAuthorizationHeader();
	        // HEADER: Get the access token from the header
	        if (!empty($headers)) {
	            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
	                return $matches[1];
	            }
	        }
	        $this->throwError( 401, 'Access Not Authorized');
		}
		
		public function verifyBearerToken($token) {
			// If token is older than 5 minutes the server will reject access
			try {
				// decode token
                
                $secret = env('TOKEN_SECRET_KEY');

				JWT::$leeway = 60; // $leeway in seconds
				$decoded = JWT::decode($token, $secret, array('HS256'));
			
				$dateCreated = end($decoded);
	
				$currentDate = date("Y-m-d H:i:s");
	
				$timeDifference = abs(strtotime($dateCreated) - strtotime($currentDate));
	
				if($timeDifference > 300) {
					$this->throwError( 401, 'Access Not Authorized');
				}

            }catch(Exception $error) {
                $this->throwError( 401, 'Access Not Authorized');
            }
	        
	    }

        
        public function throwError($code, $message) {
            header("content-type: application/json");
            http_response_code($code);
			$errorMsg = json_encode(array('message' => $message));
			echo $errorMsg; exit;
		}


    }


?>