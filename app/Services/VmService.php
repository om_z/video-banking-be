<?php

  namespace App\Services;

  class VmService {

    private $url = 'https://prod-107.westus.logic.azure.com:443/workflows/b0e25dda45464c7781282122b4c0bc12/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=vCEatTUVG4S0Y7EglYhHiqFhiYY34I2vKRhGqqHiTP0';


    //initialise with db connection
    public function __construct(){}


    public function updateCustomer($customerName, $email, $taxIdType, $taxId, $phoneNumber, $passcode, $customerType) {

      $json = array(
        "Full_x0020_Name" => $customerName,
        "Email_x0020_Address" => $email,
        "ID_x0020_Type" => $taxIdType,
        "ID_x0020_Number" => $taxId,
        "Full_x0020_Address" => "",
        "Phone_x0020_Number" => $phoneNumber,
        "Applicant_x0020_Category" => $customerType,
        "Account_x0020_Number" => "",
        "Passcode" => $passcode
      );
      
      $postdata = json_encode($json);
      
      $ch = curl_init($this->url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      $result = curl_exec($ch);
      
      // Check HTTP status code
      if (!curl_errno($ch)) {
          switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
            case 200:
              break;
            default:
              echo 'Unexpected HTTP code: ', $http_code, "\n";
          }
        }
        
        // Close handle
        curl_close($ch);
    }


  }