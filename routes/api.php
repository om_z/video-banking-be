<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CustomerTypeController;
use App\Http\Controllers\TaxidTypeController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\RecaptchaController;
use App\Http\Controllers\VerifyController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\PasscodeResetController;
use App\Http\Controllers\ChangeStatusController;
use App\Http\Controllers\ForgotPasscodeController;
use App\Http\Controllers\EmailController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| GET Request Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/customerType', [CustomerTypeController::class, 'index']);

Route::get('/idType', [TaxidTypeController::class, 'index']);

Route::get('/country', [CountryController::class, 'index']);


/*
|--------------------------------------------------------------------------
| POST Request Routes
|--------------------------------------------------------------------------
|
*/

Route::post('/recaptcha', [RecaptchaController::class, 'getCaptchaStatus']);

Route::post('/register', [RegistrationController::class, 'store']);

Route::post('/verify', [VerifyController::class, 'verifyUser']);

Route::post('/authenticate', [AuthenticationController::class, 'authenticateUser']);

Route::post('/resetPasscode', [PasscodeResetController::class, 'resetUser']);

Route::post('/generatePasscode', [ForgotPasscodeController::class, 'verify']);

Route::middleware(['basicAuth'])->group(function () {
    //All the routes are placed in here are protected by basic auth
    Route::post('/changeStatus', [ChangeStatusController::class, 'update']);
    Route::post('/sendMail', [EmailController::class, 'send']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
